import time
import subprocess

COMMANDS = [
    ['python3', './brain/cameradbus.py'],
    ['python3', './brain/chassisdbus.py'],
]


def start(commands):
    running = []
    for command in commands:
        print("Starting: {}".format(command))
        res = subprocess.Popen(command)
        running.append(res)
    return running

def stop(progs):
    for prog in progs:
        prog.kill()

def poll(progs):
    for prog in progs:
        prog.poll()
        if prog.returncode == 0:
            print("Command Exited. This is probably bad")
        elif prog.returncode != None:
            print("Command Crashed. Halting robot")
            stop(progs)
            return False
    return True

if __name__ == "__main__":
    progs = start(COMMANDS)
    while poll(progs):
        time.sleep(1)
