/* Sweep
 by BARRAGAN <rpp://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 rpp://www.arduino.cc/en/Tutorial/Sweep
*/

const char* module_type = "wheel_base";


#include <Servo.h>
#include <stdint.h>
#include "arduino_rpp/serial_rpp.h"
#include "arduino_rpp/meta.h"
#include "base.h"

void setup() {
  initSerialRpp((uint16_t)115200);
  initMeta();
  initBase();
}

void loop() {
  while(1){
        updateSerialRpp();
        updateBase();
    }
}

