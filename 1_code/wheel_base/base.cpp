#include "side_drive.h"
#include "arduino_rpp/serial_rpp.h"
#include <Arduino.h>

SideDrive LeftMotors(5, "LeftMotors");
SideDrive RightMotors(6, "RightMotors");
#define BATTERY_SENSE A3
#define ENABLED_LED 10
#define LASER_PIN 3


float getBatteryVoltageFloat(){
    int raw = analogRead(BATTERY_SENSE);
    return (float)raw * 0.048 - 1.95;
}


rppResponse putEnabledActuator(String state){
    rppResponse response = {RESPONSE_OK, state};
    if (state == TRUE){
        LeftMotors.setEnabled(true);
        RightMotors.setEnabled(true);
        digitalWrite(ENABLED_LED, HIGH);
    } else if (state == FALSE){
        LeftMotors.setEnabled(false);
        RightMotors.setEnabled(false);
        digitalWrite(ENABLED_LED, LOW);
    } else {
        response.type = RESPONSE_NOT_ACCEPTABLE;
        response.message = VALUE_ERROR;
    }
    return response;
}

rppResponse getEnabledActuator(String body){
    rppResponse response = {RESPONSE_OK, ""};
    if (LeftMotors.getEnabled() == true){
        response.message = TRUE;
    } else {
        response.message = FALSE;
    }
    return response;
}

rppResponse getThrottle(SideDrive motor, String body){
    rppResponse response = {RESPONSE_OK, ""};
    response.message = motor.getThrottle();
    return response;
}

rppResponse putThrottle(SideDrive motor, String body){
    rppResponse response = {RESPONSE_OK, ""};
    int val = body.toInt();
    if (val >= 0 && val <= 255){
        motor.setThrottle(val);
        response.message = motor.getThrottle();
    } else {
        response.type = RESPONSE_RANGE_NOT_SATISFIABLE;
        response.message = VALUE_ERROR;
    }
    return response;
}

rppResponse getLeftThrottle(String body){
    return getThrottle(LeftMotors, body);
}
rppResponse getRightThrottle(String body){
    return getThrottle(RightMotors, body);
}
rppResponse putLeftThrottle(String body){
    return putThrottle(LeftMotors, body);
}
rppResponse putRightThrottle(String body){
    return putThrottle(RightMotors, body);
}

rppResponse getBatteryVoltage(String body){
    rppResponse response = {RESPONSE_OK, ""};
    response.message += getBatteryVoltageFloat();
    response.message += "volts";
    return response;
}

rppResponse getLaserEnabled(String body){
    rppResponse response = {RESPONSE_OK, ""};
    if (digitalRead(LASER_PIN) == HIGH){
        response.message += TRUE;
    } else {
        response.message += FALSE;
    }
    return response;
}

rppResponse putLaserEnabled(String body){
    rppResponse response = {RESPONSE_OK, ""};
    if (body == TRUE){
        digitalWrite(LASER_PIN, HIGH);
        return getLaserEnabled("");
    } else if (body == FALSE) {
        digitalWrite(LASER_PIN, LOW);
        return getLaserEnabled("");
    }
    response.message = VALUE_ERROR;
    return response;
}


void initBase(void){
    LeftMotors.init();
    LeftMotors.init();
    pinMode(ENABLED_LED, OUTPUT);
    pinMode(LASER_PIN, OUTPUT);
    registerSerialResource("/drive/enabled", &getEnabledActuator, &putEnabledActuator);
    registerSerialResource("/drive/left_throttle", &getLeftThrottle, &putLeftThrottle);
    registerSerialResource("/drive/right_throttle", &getRightThrottle, &putRightThrottle);
    registerSerialResource("/laser/enabled", &getLaserEnabled, &putLaserEnabled);
    registerSerialResource("/power/battery_voltage", &getBatteryVoltage, &notAllowedHandler);

}

void updateBase(void){
}
