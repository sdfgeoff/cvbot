#include "side_drive.h"
#include "arduino_rpp/config.h"

SideDrive::SideDrive(int pin, const char* name) {
    /* Happens at compile time */
    SideDrive::throttle = 0;
    SideDrive::enabled = false;
    SideDrive::pin = pin;
    SideDrive::name = name;
}

void SideDrive::init(void){
    /* Happens at run-time */
    SideDrive::enabled = false;
    SideDrive::throttle = 0;
    pinMode(SideDrive::pin, OUTPUT);
    digitalWrite(SideDrive::pin, LOW);
}


void SideDrive::setThrottle(int newThrottle) {
    SideDrive::throttle = newThrottle;
    if (SideDrive::enabled){
        analogWrite(SideDrive::pin, SideDrive::throttle);
    }
}

int SideDrive::getThrottle(void){
    return SideDrive::throttle;
}


void SideDrive::setEnabled(bool state){
    if (state == true){
        analogWrite(SideDrive::pin, SideDrive::throttle);
    } else {
        digitalWrite(SideDrive::pin, LOW);
    }
    SideDrive::enabled = state;
}

bool SideDrive::getEnabled(void){
    return SideDrive::enabled;
}

const char* SideDrive::getName(void){
    return SideDrive::name;
}

