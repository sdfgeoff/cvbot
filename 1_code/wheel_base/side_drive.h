#ifndef SERVO_DRIVE
#define SERVO_DRIVE
#include <stdint.h>

class SideDrive {
    private:
        int16_t throttle;
        int8_t center;
        const char* name;

        bool enabled;
        uint8_t pin;

    public:
        SideDrive(int pin, const char* name);
        void init(void);
        void setThrottle(int newThrottle);
        int getThrottle(void);
        void setEnabled(bool state);
        bool getEnabled(void);
        const char* getName(void);

};
#endif
