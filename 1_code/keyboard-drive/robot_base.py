import sys
import time

CAM_RESOLUTION = (640, 480)

class Base(object):
    '''The wheeled robot base'''
    def __init__(self, serial_port, baud):
        print("Initalizing Robot Base")
        print("    Communications Bus:            ", end='')
        sys.stdout.flush()
        import pyrpp
        print("   [OK]")
        print("    Connecting To Chassis          ", end='')
        sys.stdout.flush()
        try:
            self.robot = pyrpp.module.RppModule(serial_port, baud)
            print("   [OK]")
        except:
            print(" [FAIL]")
            self.robot = None

        if self.robot is not None:
            print("    Enabling Power                 ", end='')
            sys.stdout.flush()
            try:
                self.robot['drive']['enabled'] = True
                print("   [OK]")
            except:
                print(" [FAIL]")

        self.camera = Camera()
        print("Robot Ready")


    def __del__(self):
        print("Terminating Robot Base")
        if self.robot is not None:
            print("    Turning Power Off              ", end='')
            sys.stdout.flush()
            try:
                self.robot['drive']['enabled'] = False
                self.robot['laser']['enabled'] = False
                print("   [OK]")
            except:
                print(" [FAIL]")
            print("    Disconnecting Chassis          ", end='')
            sys.stdout.flush()
            self.robot.client.close()
            print("   [OK]")

    def drive(self, speed, turn):
        '''Expects values in floats from 0-1'''
        left_speed = int(speed + turn)*254
        right_speed = int(speed - turn)*254
        self.robot['drive']['left_throttle'] = max(0, left_speed)
        self.robot['drive']['right_throttle'] = max(0, right_speed)

    def set_laser(self, val):
        self.robot['laser']['enabled'] = val

    def get_laser(self):
        return self.robot['laser']['enabled'] == 'True'

    def get_battery_voltage(self):
        raw = self.robot['power']['battery_voltage'].lower()
        val = float(raw.split('v')[0])
        return val




class Camera(object):
    '''A wrapper around picamera that dumps into cv2'''
    def __init__(self):
        print("Initalizing Camera Module")
        print("    Camera Subsystem               ", end="")
        sys.stdout.flush()
        try:
            from picamera import PiCamera
            from picamera.array import PiRGBArray
            print("   [OK]")
        except:
            print(" [FAIL]")
            return

        print("    Image Processing Subsystem     ", end="")
        sys.stdout.flush()
        import cv2
        globals()['cv2'] = cv2
        print("   [OK]")
        print("    Connecting To Camera           ", end="")
        self.camera = PiCamera()
        self.camera.vflip = True
        self.camera.hflip = True
        self.camera.resolution = CAM_RESOLUTION
        self.raw_capture = PiRGBArray(self.camera)
        print("   [OK]")

    def get_image(self):
        self.camera.capture(self.raw_capture, format="rgb")
        image = self.raw_capture.array
        self.raw_capture.truncate(0)
        return image

    def display_image(self):
        image = self.get_image()
        cv2.imshow("Image", image)
        cv2.waitKey(0)

    def save_image(self):
        image = self.get_image()
        print("Saving Image")
        filename = time.strftime("%y-%m-%d %H:%M:%S", time.gmtime()) + ".png"
        cv2.imwrite(filename,image)


