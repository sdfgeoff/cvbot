#!/usr/bin/python
import sys
import time
import robot_base

CAMERA_RES = (1024, 768)


def input_char():
    '''Blocking character read'''
    import tty
    import termios
    fdes = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fdes)
    try:
        tty.setraw(sys.stdin.fileno())
        char = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fdes, termios.TCSADRAIN, old_settings)
    return char


def boot():
    '''Starts up the system'''
    robot = robot_base.Base("/dev/ttyACM0", 115200)
    loop(robot)


def loop(robot):
    '''Loops allowing keypresses to drive'''
    keypress = ''
    while keypress != 'q':
        keypress = input_char()
        if keypress == 'w':
            robot.drive(1, 0)
        elif keypress == 's':
            robot.drive(0, 0)
        elif keypress == 'd':
            robot.drive(0, 1)
        elif keypress == 'a':
            robot.drive(0, -1)


        elif keypress == 'l':
            old_val = robot.get_laser()
            robot.set_laser(not old_val)

        elif keypress == 'p':
            robot.camera.save_image()

        print(robot.get_battery_voltage(), end="                   \r")


if __name__ == "__main__":
    boot()
