#!/usr/bin/python
from gi.repository import Gtk, GLib, GObject
from datetime import datetime

class MainWindow(Gtk.Window):
    def __init__(self):
        self.startclocktimer()

    # Displays Timer
    def displayclock(self):
        #  putting our datetime into a var and setting our label to the result. 
        #  we need to return "True" to ensure the timer continues to run, otherwise it will only run once.
        print("HERE")
        return True

    # Initialize Timer
    def startclocktimer(self):
        #  this takes 2 args: (how often to update in millisec, the method to run)
        GObject.timeout_add(1000, self.displayclock)


win = MainWindow()
GLib.MainLoop().run()