import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib, GObject

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()


class Laser(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('robot.sensors.vision', bus=bus)
        dbus.service.Object.__init__(self, bus_name, '/camera')

    @dbus.service.method('the.camera.config', in_signature='i')
    def runme(self, i):
        print("I was called with {}".format(i))

myservice = Laser()
loop = GLib.MainLoop()
loop.run()