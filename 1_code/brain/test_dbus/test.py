from dbus.mainloop.glib import DBusGMainLoop
from dbus.service import signal
import dbus
from gi.repository import GLib, GObject
import time


DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()

class Sender(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('robot.sensors.vision', bus=bus)
        dbus.service.Object.__init__(self, bus_name, "/raw_data")
        self.fps = 1000
        self.timer_id = GObject.timeout_add(1000/self.fps, self.update)
        self.counter = 0

        
    @signal(dbus_interface='com.example.Sample', signature='s')
    def laser(self, d):
        print(time.time())

    def update(self):
        self.counter += 1
        self.laser("a"*10)
        return self.counter < 100


loop = GLib.MainLoop()
s = Sender()
loop.run()