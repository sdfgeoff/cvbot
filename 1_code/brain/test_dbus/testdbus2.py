import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib, GObject

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()


class Laser(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('robot.sensors.laser', bus=bus)
        dbus.service.Object.__init__(self, bus_name, '/distance')

        print(GObject.timeout_add(16, self.update))
        #print(dir(GObject))

    @dbus.service.method('the.laser.config')
    def hello(self):
        return "Hello,World!"

    def update(self):
        print("Updating")
        return True

myservice = Laser()
loop = GLib.MainLoop()
loop.run()