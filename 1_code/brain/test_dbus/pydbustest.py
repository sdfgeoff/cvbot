from pydbus import SystemBus
from gi.repository import GLib

bus = SystemBus()
print(dir(bus))
systemd = bus.get(".systemd1")

systemd.JobNew.connect(print)
GLib.MainLoop().run()
