from dbus.mainloop.glib import DBusGMainLoop
import dbus
from gi.repository import GLib
import time

DBusGMainLoop(set_as_default=True)
session_bus = dbus.SessionBus()

counter = 0
def on_laser(line, quality):
    global counter
    print(counter)
    counter += 1

session_bus.add_signal_receiver(on_laser, signal_name="laser_line")

loop = GLib.MainLoop()
loop.run()