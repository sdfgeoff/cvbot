import dbus
import dbus.service
import camera
import time
import logging
from dbus.mainloop.glib import DBusGMainLoop
from dbus.service import signal, method
from gi.repository import GLib, GObject

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()


class Camera(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('robot.sensors.camera', bus=bus)
        dbus.service.Object.__init__(self, bus_name, '/camera')

        self.fps = 60
        self.timer_id = GObject.timeout_add(int(1000/self.fps), self.update)

        self.actual_fps = 0
        self._last_time = time.time()

    def __enter__(self):
        print("Starting Camera")

    @signal(dbus_interface='the.camera.interface')
    def laser_line(self, laser_line, line_quality):
        pass

    @method(dbus_interface='the.camera.interface', in_signature='i')
    def set_target_framerate(self, fps):
        logging.info("Setting Framerate to %i", fps)
        self.fps = fps
        GObject.source_remove(self.timer_id)
        self.timer_id = GObject.timeout_add(int(1000/fps), self.update)
        return self.fps

    @method(dbus_interface='the.camera.interface')
    def get_actual_framerate(self):
        return self.actual_fps

    @method(dbus_interface='the.camera.interface', in_signature='s')
    def save_image(self, filepath):
        camera.save_image(filepath)

    def __exit__(self):
        print("Closing Camera")


    def update(self):
        line, quality = camera.read_laser()
        self.laser_line(dbus.Array(line), dbus.Array(quality))
        current_time = time.time()
        self.actual_fps = 1/(current_time - self._last_time)
        self._last_time = current_time
        return True


if __name__ == "__main__":
    with Camera() as cam:
        loop = GLib.MainLoop()
        loop.run()
