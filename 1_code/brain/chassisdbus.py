import dbus
import dbus.service
import chassis
import time
import logging
from dbus.mainloop.glib import DBusGMainLoop
from dbus.service import signal, method
from gi.repository import GLib, GObject

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()


class Chassis(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('robot.actuators.chassis', bus=bus)
        dbus.service.Object.__init__(self, bus_name, '/chassis')

        bus.add_signal_receiver(self.on_drive_command, signal_name="drive")
        self.chassis = None
        self.timer_id = GObject.timeout_add(int(1000), self.update)

    def __enter__(self):
        print("STARTING CHASSIS")
        self.chassis = chassis.Chassis('/dev/ttyACM0', 115200)

    def on_drive_command(self, speed, steer):
        '''Runs whenever something else sends a signal with the name "drive"
        The numbers should be a percentage'''
        self.chassis.drive(speed, steer)

    @signal(dbus_interface='the.chassis.interface')
    def battery_level(self, percent):
        pass

    @method(dbus_interface='the.chassis.interface', in_signature='b')
    def set_laser_on(self, state):
        logging.info("Setting laser enabled to %i", fps)
        self.chassis.laser_on = state
        return self.fps

    @method(dbus_interface='the.chassis.interface')
    def set_laser_on(self, state):
        return self.chassis.laser_on

    def update(self):
        try:
            battery_voltage = self.chassis.get_battery_voltage()
            self.battery_level((battery_voltage-6.0)/2.0)
        except:
            pass
        else:
            if battery_voltage < 6.0:
                self.chassis.drive_enabled = False
        return True

    def __exit__(self):
        print("CLEANING UP")
        del self.chassis

if __name__ == "__main__":
    with Chassis() as cha:
        loop = GLib.MainLoop()
        loop.run()
