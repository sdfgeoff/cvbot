## Intro
This module provides a python interface for RPP (Robot Peripheral Protocol).
allowing a robot to interface with it's hardware through python. This module
has been used in ROS as well as in standalone application.

This is both python2 and python3 compatible

## Dependencies

* pyserial

## Basic Use

Using the base client:
```
import pyrpp

a = pyrpp.client.RppClient('/dev/ttyUSB0', 115200)
print(a.get('/meta/name'))
a.put('/power/enabled', 1)
```

Using the module:
```
import pyrpp

a = pyrpp.module.RppModule('/dev/ttyACM0', 115200)
print(a['meta']['name']
a['power']['enabled'] = 'True'
```

## Future development
Due to the asynchronus nature of communications to/from the peripherals, I want
to add OnChange callbacks to RPPModule