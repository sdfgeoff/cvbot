'''This module provides a generic interface to an RTTP module through
a dictionary-like structure. eg:
    module = GenericModule()
    module['meta']['name'] = 'setName'
    module['power']['enabled'] = 'True'
    print(module['power']['battery_voltage'])
'''

from . import client
import time


class RppModule(object):
    '''A generic dictionary-accessable module'''
    def __init__(self, port, baud, timeout=1.5):
        self.client = client.RppClient(port, baud, timeout)
        self.name = self.client.name
        self.listing = self._build_directory_listing()

    def _build_directory_listing(self, base_path='/'):
        '''Recursively searches through the module to build a tree'''
        raw = self.client.get(base_path).body.split('\n')
        tree = dict()
        for entry in raw:
            if entry == '':
                continue
            if entry.endswith('/'):
                out = self._build_directory_listing(entry)  # Recurse on branch
                tree[entry.split('/')[1]] = out
            else:
                tree[entry.split('/')[-1]] = ''
        return tree

    def close(self):
        self.client.close()

    def checkAsync(self):
        return self.client.read(0.05)

    def repopulate_tree(self):
        '''Rebuilds the directory listing'''
        self.listing = self._build_directory_listing()

    def keys(self):
        return self.listing.keys()

    def __str__(self):
        return "GenericModule: {}".format(self.name)

    def __getitem__(self, key):
        if key in self.listing:
            if isinstance(self.listing[key], dict):
                return RecursiveMagic(self, key)
            else:
                return self.client.get('/'+key)
        else:
            raise KeyError(key)

    def __setitem__(self, key, content):
        if key in self.listing:
            if isinstance(self.listing[key], dict):
                # Allow putting to directories? - debatable
                return self.client.put('/'+key+'/', content).body
            else:
                return self.client.put('/'+key, content).body
        else:
            raise KeyError(key)


class RecursiveMagic(client.RppMessage):
    '''A class that recursively searches it's parent's listing'''
    def __init__(self, parent, base_path):

        self.parent = parent
        self.base_path = base_path
        subtree = self.parent.listing
        for i in self.base_path.split('/'):
            subtree = subtree[i]
        body = '\n'.join(list(subtree.keys()))
        super(RecursiveMagic, self).__init__(200, '/' + base_path, body)

    def keys(self):
        subtree = self.parent.listing
        for i in self.base_path.split('/'):
            subtree = subtree[i]
        return subtree.keys()

    def __getitem__(self, key):
        subtree = self.parent.listing
        for i in self.base_path.split('/'):
            subtree = subtree[i]
        if key in subtree:
            if isinstance(subtree[key], dict):
                return RecursiveMagic(self, self.base_path + '/' + key).body
            else:
                return self.parent.client.get('/' + self.base_path + '/' + key).body
        else:
            raise KeyError(key)

    def __setitem__(self, key, content):
        subtree = self.parent.listing
        for i in self.base_path.split('/'):
            subtree = subtree[i]
        if key in subtree:
            if isinstance(subtree[key], dict):
                self.parent.client.put('/'+self.base_path+'/'+key+'/', content)
            else:
                self.parent.client.put('/'+self.base_path+'/'+key, content)
        else:
            raise KeyError(key)


    

if __name__ == "__main__":
    a = RppModule('/dev/ttyACM0', 115200)
    a['power']['enabled'] = 'True'
