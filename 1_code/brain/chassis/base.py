import sys
import time
from . import pyrpp

class Chassis(object):
    '''The wheeled robot base'''
    def __init__(self, serial_port, baud):
        self.robot = None
        self.robot = pyrpp.module.RppModule(serial_port, baud)

        self.drive_enabled = True
        self.laser_on = True

    @property
    def drive_enabled(self):
        self.robot['drive']['enabled'] = True

    @drive_enabled.setter
    def drive_enabled(self, val):
        self.robot['drive']['enabled'] = val

    def drive(self, speed, turn):
        '''Expects values in floats from 0-1'''
        left_speed = int(speed + turn)*254
        right_speed = int(speed - turn)*254
        self.robot['drive']['left_throttle'] = max(0, left_speed)
        self.robot['drive']['right_throttle'] = max(0, right_speed)

    def __del__(self):
        print("CLOSING CHASSIS")
        if self.robot != None:
            self.robot['drive']['enabled'] = False
            self.robot['laser']['enabled'] = False
            self.robot.client.close()
            self.robot = None

    @property
    def laser_on(self):
        return self.robot['laser']['enabled'] == 'True'

    @laser_on.setter
    def laser_on(self, val):
        self.robot['laser']['enabled'] = val

    def get_battery_voltage(self):
        raw = self.robot['power']['battery_voltage'].lower()
        val = float(raw.split('v')[0])
        return val

