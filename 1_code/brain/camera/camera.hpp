#include <inttypes.h>

#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480

#define CAM_H_FOV 1.085 // math.radians(62.2)
#define CAM_V_FOV 0.851 // math.radians(48.8)  
#define LASER_OFFSET 6.0  // Vertical space between camera and laser

#define DEGREES_PER_PIXEL (CAM_V_FOV / IMAGE_HEIGHT)

extern "C" void initCamera(void);
extern "C" void captureImage(void);
extern "C" void saveImage(char filePath[]);
extern "C" void loadImage(char filePath[]);

extern "C" void filterImage();

extern "C" void extractLaser(uint16_t laser[IMAGE_WIDTH], uint16_t quality[IMAGE_WIDTH]);
extern "C" void line_to_distance(uint16_t laser[IMAGE_WIDTH]);
