import fix_path
import os
import camera

_current_dir = os.path.split(os.path.abspath(__file__))[0]
_test_img_directory = os.path.normpath(os.path.join(_current_dir, "test_images"))

def run_test_images():
    """Opens all images in the test image directory"""
    for img in sorted(os.listdir(_test_img_directory), reverse=True):
        path = os.path.join(_test_img_directory, img)
        camera.test_image(path)


if __name__ == "__main__":
    run_test_images()
