import fix_path
import camera
import time
import cProfile

prof = cProfile.Profile()


for i in range(60):
    prof.runcall(camera.read_laser)

prof.print_stats()
