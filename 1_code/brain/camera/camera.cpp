// display the difference between adjacent video frames (press any key to exit)
#include "camera.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

volatile uint16_t image_width = IMAGE_WIDTH;
volatile uint16_t image_height = IMAGE_HEIGHT;
volatile uint16_t cam_h_fov = CAM_H_FOV;
volatile uint16_t cam_v_fov = CAM_V_FOV;

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

Mat image;
VideoCapture capture(0); // connect to the webcam
Size2i size(IMAGE_WIDTH,IMAGE_HEIGHT);

Vec3b target_color = {0, 0, 0};
uint8_t max_target_difference = 0;

// ------------------------ INTERNAL FUNCTIONS ---------------------------

inline uint16_t compare_colors(Vec3b current_color){
    // Requrns the "laser quality" of the current pixel. Lower = better
    // uint16_t y = (255 - current_color[0]); // Y
    uint16_t u = (-120 + current_color[1]); // U
    uint16_t v = (-220 + current_color[2]); // V

    return u*u + v*v;
}


// ------------------------ PUBLIC FUNCTIONS -----------------------------

extern "C" void initCamera() {
    capture.set(CV_CAP_PROP_FRAME_WIDTH,IMAGE_WIDTH);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT,IMAGE_HEIGHT);
    capture.grab();
}


extern "C" void captureImage() {
    // Samples an image from the camera
    //capture >> image;
    capture.retrieve(image);
    capture.grab();
}

extern "C" void saveImage(char path[]) {
    imwrite(path, image);
}


extern "C" void loadImage(char filePath[]) {
    // Loads an image from disk (for testing)
    Mat full;
    printf("Loading image from %s\n", filePath);
    full = imread(filePath, CV_LOAD_IMAGE_COLOR);
    resize(full, image, size);
}

extern "C" void filterImage(){
    //Pre-filter the image
    cvtColor(image, image, cv::COLOR_BGR2YUV);  // Convert to YUV
}


extern "C" void extractLaser(uint16_t laser[IMAGE_WIDTH], uint16_t quality[IMAGE_WIDTH]) {
    // Fills two buffers, the first with the "best guess" for where the laser
    // is, the second with how good the guess is.
    Vec3b current_color; // Y, Cr, Cb

    uint16_t current_x = 0;
    uint16_t current_y = 0;

    uint16_t current_quality = 0;


    for (current_x = 0; current_x < IMAGE_WIDTH; current_x++){
        quality[current_x] = -1;  // Ensure set to min quality
        laser[current_x] = IMAGE_HEIGHT/2;

        // For each pixel in the column, find the laser
        for (current_y = IMAGE_HEIGHT-1; current_y > IMAGE_HEIGHT/2; current_y--){
            current_color = image.at<Vec3b>(Point(current_x,current_y));
            current_quality = compare_colors(current_color);

            // Weighting to lines lower in the image
            current_quality += (IMAGE_HEIGHT - current_y) * 300 / IMAGE_HEIGHT;

            // Weight to close to the previous line
            if (current_x > 1){
                current_quality += abs(laser[current_x-1] - current_y) * max(0, 100 - quality[current_x-1]);
            }

            if (current_quality < quality[current_x]){
                quality[current_x] = current_quality;
                laser[current_x] = current_y;
            }
        }
    }

}

extern "C" void line_to_distance(uint16_t laser[IMAGE_WIDTH]){
    //Converts from pixel-space into distance
    for (uint16_t current_x = 0; current_x < IMAGE_WIDTH; current_x++){
        float angular = DEGREES_PER_PIXEL * (laser[current_x] - IMAGE_HEIGHT/2);
        float lin_dist = LASER_OFFSET/(tan(angular)+0.01);
        float proper_dist = lin_dist * (2.0 - cos((float)current_x/IMAGE_WIDTH * CAM_H_FOV - CAM_H_FOV/2));
        laser[current_x] = proper_dist;
    }
}




