import os
import ctypes
import logging
import math
import collections

_current_dir = os.path.split(os.path.abspath(__file__))[0]
_laser_detect_path = os.path.join(_current_dir, "camera.so")
if not os.path.exists(_laser_detect_path):
    raise Exception("Unable to find laser c interface at {}".format(_laser_detect_path))
logging.info("Loading laser accelerator from %s", _laser_detect_path)
LASER_DETECT = ctypes.cdll.LoadLibrary(_laser_detect_path)
logging.info("Loaded laser accelerator")
# Grab constants
IMAGE_WIDTH = ctypes.c_uint16.in_dll(LASER_DETECT, "image_width").value
IMAGE_HEIGHT = ctypes.c_uint16.in_dll(LASER_DETECT, "image_height").value
CAM_H_FOV = ctypes.c_uint16.in_dll(LASER_DETECT, "cam_h_fov").value
CAM_V_FOV = ctypes.c_uint16.in_dll(LASER_DETECT, "cam_v_fov").value

# Make an array for storing the laser line in
RAW_LASER_LINE = ctypes.c_uint16 * IMAGE_WIDTH

LASER_DETECT.initCamera()
logging.debug("Camera inited")


def test_image(image_path):
    """Loads image from specified path and detects/plots laser line"""
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg

    line_raw = RAW_LASER_LINE()
    quality = RAW_LASER_LINE()

    LASER_DETECT.loadImage(image_path.encode('ascii'))
    filter_image()
    extract(line_raw, quality)

    line = [l for l in line_raw]
    quality = [l for l in quality]

    horiz = range(0, IMAGE_WIDTH)

    img_plot = plt.subplot(121)
    img_plot.axis = [0, IMAGE_WIDTH, IMAGE_HEIGHT, 0]
    img_plot.plot(horiz, line, alpha=0.5)
    img_plot.fill_between(
        horiz,
        [i*IMAGE_HEIGHT/100000 for i in quality],
        alpha=0.5, color='grey'
    )
    img_plot.imshow(
        mpimg.imread(image_path),
        extent=[0, IMAGE_WIDTH, IMAGE_HEIGHT, 0]
    )

    plot_distance(line_raw)
    plt.show()


def to_distance(line):
    """Convert line in pixel-space into distance"""
    LASER_DETECT.line_to_distance(line)

def capture():
    """Read image from camera"""
    LASER_DETECT.captureImage()


def extract(line, quality):
    """Exract the laser line from the currently loaded image"""
    LASER_DETECT.extractLaser(line, quality)


def filter_image():
    """Pre-process the image"""
    LASER_DETECT.filterImage()


def read_laser():
    """Extracts the laser line from the ccamera"""
    line = RAW_LASER_LINE()
    quality = RAW_LASER_LINE()

    capture()
    filter_image()
    extract(line, quality)
    to_distance(line)
    return line, quality


def save_image(filepath):
    capture()
    filepath = os.path.abspath(filepath)
    logging.info("Saving image to %s", filepath)
    LASER_DETECT.saveImage(filepath.encode('ascii'))


def plot_distance(line):
    """Plots distance to laser line in a polar plot"""
    import matplotlib.pyplot as plt
    import numpy
    to_distance(line)
    polar_plt = plt.subplot(122, projection='polar')
    polar_plt.plot(numpy.linspace(CAM_H_FOV/2, -CAM_H_FOV/2, IMAGE_WIDTH), line)
    polar_plt.set_theta_offset(math.pi/2)
    polar_plt.set_thetamin(-90)
    polar_plt.set_thetamax(90)
    polar_plt.set_rmax(100)
